Rails.application.routes.draw do

  mount_opro_oauth
  root 'home#index'

  devise_for :users

  resources :users
end
